package cellular;

import datastructure.IGrid;
import java.util.Random;
import datastructure.CellGrid;


public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int column) {
        // TODO Auto-generated method stub
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
		    } 
        }
    }

    @Override
    public void step() {
        // TODO Auto-generated method stub
        IGrid nextGeneration = currentGeneration.copy();
		for(int m = 0; m < numberOfRows(); m++){
			for(int n = 0; n< numberOfColumns(); n++){
				CellState nextCell = getNextCell(m, n);
				nextGeneration.set(m, n, nextCell);
			}
		}
		this.currentGeneration = nextGeneration; 
    }

    @Override
    public CellState getNextCell(int row, int column) {
        // TODO Auto-generated method stub
        int Alive = countNeighbors(row,column,CellState.ALIVE);
        CellState current_state = getCellState(row, column);
		if (current_state == CellState.ALIVE) {
			return CellState.DYING;
		}
		else if (current_state == CellState.DYING) {
			return CellState.DEAD;
		}
		else if (current_state == CellState.DEAD && Alive == 2) {
			return CellState.ALIVE;
		}
		else {
			return CellState.DEAD;
		}
    }

    @Override
    public int numberOfRows() {
        // TODO Auto-generated method stub
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        // TODO Auto-generated method stub
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return currentGeneration;
    }

    private int countNeighbors(int row, int column, CellState state) {
		// TODO
		int neighbors = 0;
		for (int m = row-1; m <= row+1; m++) {
			for (int n = column-1; n <= column+1; n++) {
				if ((m < 0 || m >=numberOfRows()) || (n < 0 || n >= numberOfColumns()) || ((m == row) && (n == column))){
                    continue;
                }
                else if (getCellState(m, n) == state){
                    neighbors++;
                }
			}
		}
		return neighbors;
    
    
}
}
