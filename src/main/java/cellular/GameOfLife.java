package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columnumns) {
		currentGeneration = new CellGrid(rows, columnumns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int column = 0; column < currentGeneration.numColumns(); column++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, column, CellState.ALIVE);
				} else {
					currentGeneration.set(row, column, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO	
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int column) {
		// TODO
		return currentGeneration.get(row, column);
	}

	@Override
	public void step() {
		// TODO
		IGrid nextGeneration = currentGeneration.copy();
		for(int m = 0; m < numberOfRows(); m++){
			for(int n = 0; n< numberOfColumns(); n++){
				CellState nextCell = getNextCell(m, n);
				nextGeneration.set(m, n, nextCell);
			}
		}
		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int column) {
		// TODO
		int Alive = countNeighbors(row,column,CellState.ALIVE);
		if (Alive < 2 || Alive > 3) {
			return CellState.DEAD;
		}
		else if (Alive == 3 ) {
			return CellState.ALIVE;
		}
		else if (getCellState(row, column) == CellState.ALIVE) {
			return CellState.ALIVE;
		}
		else {
			return CellState.DEAD;
		}
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, column) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int column, CellState state) {
		// TODO
		int neighbors = 0;
		for (int m = row-1; m <= row+1; m++) {
			for (int n = column-1; n <= column+1; n++) {
				if ((m < 0 || m >=numberOfRows()) || (n < 0 || n >= numberOfColumns()) || ((m == row) && (n == column))){
                    continue;
                }
                else if (getCellState(m, n) == state){
                    neighbors++;
                }
			}
		}
		return neighbors;
		// TODO
	}
	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
