package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private cellular.CellState[][] CellState;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        CellState = new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row < numRows() && row >= 0){
            if(column < numColumns() && column >= 0){
                CellState[row][column] = element;
            }
            else{
                throw new IndexOutOfBoundsException();
            }
        }
        else{
            throw new IndexOutOfBoundsException();
        }
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row < numRows() && row >= 0){
            if(column < numColumns() && column >= 0){
                return CellState[row][column];
            }
            else{
                throw new IndexOutOfBoundsException();
            }
        }
        else{
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid Grid_copy = new CellGrid(rows, columns, cellular.CellState.DEAD);
            for(int m = 0; m<rows; m++){
                for(int n = 0; n<columns; n++){
                    Grid_copy.set(m,n, get(m,n));
                }
            }
        return Grid_copy;
    }
    
}
